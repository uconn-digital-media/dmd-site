const tailwind = require('./tailwind')
const env = process.env.NODE_ENV !== 'production' ? require('./config/development.js') : require('./config/production.js')

module.exports = {
	/*
	** Headers of the page
	*/
	head: {
		htmlAttrs: {
			lang: 'en',
		},
		title: 'UConn DMD',
		meta: [
			{charset: 'utf-8'},
			{name: 'viewport', content: 'width=device-width, initial-scale=1'},
			{hid: 'description', name: 'description', content: 'The UConn Department of Digital Media and Design'}
		],
		link: [
			{rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'},
			{rel: 'stylesheet', href: 'https://use.typekit.net/aao0ozq.css'}
		]
	},
	css: ['~/assets/styles/app.css'],
	plugins: [
		{src: '~/plugins/smoothscroll.js', ssr: false},
	],
	env: env,
	modules: [
		'@nuxtjs/axios',
	],
	axios: {
		proxyHeaders: true,
	},
	/*
	** Customize the progress bar color
	*/
	loading: {
		color: tailwind.colors['blue'],
		failedColor: tailwind.colors['red'],
		height: '3px',
		duration: 3500,
	},
	/*
	** Build configuration
	*/
	build: {
		/*
		** Run ESLint on save
		*/
		extend(config, {isDev, isClient}) {
			if (isDev && isClient) {
				config.module.rules.push({
					enforce: 'pre',
					test: /\.(js|vue)$/,
					loader: 'eslint-loader',
					exclude: /(node_modules)/
				})
			}
			if (!isDev) {
				// Remove unused CSS using purgecss. See https://github.com/FullHuman/purgecss
				// for more information about purgecss.
				// config.plugins.push(
				// new PurgecssPlugin({
				// 	paths: glob.sync([
				// 		path.join(__dirname, './pages/**/*.vue'),
				// 		path.join(__dirname, './layouts/**/*.vue'),
				// 		path.join(__dirname, './components/**/*.vue')
				// 	]),
				// 	whitelist: ['html', 'body'],
				// 	extractors: [
				// 		{
				// 			extractor: TailwindExtractor,
				// 			extensions: ["vue"]
				// 		}
				// 	]
				// })
				// )
			}
		},
		extractCSS: true,
		postcss: [
			require('tailwindcss')('./tailwind.js'),
			require('autoprefixer'),
		],
		vendor: [
			'~/tailwind.js',
		]
	},
	/*
	** Router
	*/
	router: {
		scrollBehavior: (to, from, savedPosition) => {
			// if the returned position is falsy or an empty object,
			// will retain current scroll position.
			let position = false

			// if no children detected
			if (to.matched.length < 2) {
				// scroll to the top of the page
				position = {x: 0, y: 0}
			} else if (to.matched.some((r) => r.components.default.options.scrollToTop)) {
				// if one of the children has scrollToTop option set to true
				position = {x: 0, y: 0}
			}

			// savedPosition is only available for popstate navigations (back button)
			if (savedPosition) {
				position = savedPosition
			}

			return new Promise(resolve => {
				// wait for the out transition to complete (if necessary)
				window.$nuxt.$once('triggerScroll', () => {
					// coords will be used if no selector is provided,
					// or if the selector didn't match any element.
					if (to.hash && document.querySelector(to.hash)) {
						// scroll to anchor by returning the selector
						position = {selector: to.hash, offset: {x: 0, y: 200}}
					}
					resolve(position)
				})
			})
		}
	}
}
