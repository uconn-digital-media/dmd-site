const imageURL = {
	methods: {
		imageURL(path) {
			return process.env.SERVER_URL + path
		}
	}
}

export default imageURL