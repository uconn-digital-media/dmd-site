const ScrollEvents = {
	props: {
		seEnterOffset: {
			type: Number,
			required: false,
			default: 0,
		}
	},
	data() {
		return {
			se_yPos: 0,
			se_raf: null,
		}
	},
	methods: {
		se_update() {
			if (window.pageYOffset + window.innerHeight + this.seEnterOffset > this.se_yPos) {
				this.se_fire()
				window.removeEventListener('resize', this.resize, false)
				this.se_raf = null
			} else {
				this.se_raf = window.requestAnimationFrame(this.se_update.bind(this))
			}
		},
		se_resize() {
			this.se_yPos = window.pageYOffset + this.$el.getBoundingClientRect().top
		},
		se_init() {
			this.se_resize()
			window.addEventListener('resize', this.se_resize, false)
			this.se_raf = window.requestAnimationFrame(this.se_update.bind(this))
		},
		se_destroy() {
			window.removeEventListener('resize', this.se_resize, false)
			window.cancelAnimationFrame(this.se_raf)
		}
	},
}

export default ScrollEvents