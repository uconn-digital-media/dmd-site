/**
 * @author Mugen87 / https://github.com/Mugen87
 * @author mrdoob / http://mrdoob.com/
 */

import {
	Mesh,
	MeshBasicMaterial,
	Vector3,
	Vector2,
	Box2,
	Vector4,
	Color,
	DataTexture,
	RGBFormat,
	NearestFilter,
	ClampToEdgeWrapping,
	RawShaderMaterial,
	AdditiveBlending,
	BufferGeometry,
	InterleavedBuffer,
	InterleavedBufferAttribute
} from 'three'

class Lensflare extends Mesh {
	constructor() {
		super(Lensflare.Geometry, new MeshBasicMaterial({opacity: 0, transparent: true}));

		this.type = 'Lensflare';
		this.frustumCulled = false;
		this.renderOrder = Infinity;

		//

		this.positionScreen = new Vector3();

		// textures

		this.tempMap = new DataTexture(new Uint8Array(16 * 16 * 3), 16, 16, RGBFormat);
		this.tempMap.minFilter = NearestFilter;
		this.tempMap.magFilter = NearestFilter;
		this.tempMap.wrapS = ClampToEdgeWrapping;
		this.tempMap.wrapT = ClampToEdgeWrapping;
		this.tempMap.needsUpdate = true;

		this.occlusionMap = new DataTexture(new Uint8Array(16 * 16 * 3), 16, 16, RGBFormat);
		this.occlusionMap.minFilter = NearestFilter;
		this.occlusionMap.magFilter = NearestFilter;
		this.occlusionMap.wrapS = ClampToEdgeWrapping;
		this.occlusionMap.wrapT = ClampToEdgeWrapping;
		this.occlusionMap.needsUpdate = true;

		// material

		this.geometry = Lensflare.Geometry();

		this.material1a = new RawShaderMaterial({
			uniforms: {
				'scale': {value: null},
				'screenPosition': {value: null}
			},
			vertexShader: [

				'precision highp float;',

				'uniform vec3 screenPosition;',
				'uniform vec2 scale;',

				'attribute vec3 position;',

				'void main() {',

				'	gl_Position = vec4( position.xy * scale + screenPosition.xy, screenPosition.z, 1.0 );',

				'}'

			].join('\n'),
			fragmentShader: [

				'precision highp float;',

				'void main() {',

				'	gl_FragColor = vec4( 1.0, 0.0, 1.0, 1.0 );',

				'}'

			].join('\n'),
			depthTest: true,
			depthWrite: false,
			transparent: false
		});

		this.material1b = new RawShaderMaterial({
			uniforms: {
				'map': {value: this.tempMap},
				'scale': {value: null},
				'screenPosition': {value: null}
			},
			vertexShader: [

				'precision highp float;',

				'uniform vec3 screenPosition;',
				'uniform vec2 scale;',

				'attribute vec3 position;',
				'attribute vec2 uv;',

				'varying vec2 vUV;',

				'void main() {',

				'	vUV = uv;',

				'	gl_Position = vec4( position.xy * scale + screenPosition.xy, screenPosition.z, 1.0 );',

				'}'

			].join('\n'),
			fragmentShader: [

				'precision highp float;',

				'uniform sampler2D map;',

				'varying vec2 vUV;',

				'void main() {',

				'	gl_FragColor = texture2D( map, vUV );',

				'}'

			].join('\n'),
			depthTest: false,
			depthWrite: false,
			transparent: false
		});

		// the following object is used for occlusionMap generation

		this.mesh1 = new Mesh(this.geometry, this.material1a);

		//

		this.elements = [];

		this.shader = LensflareElement.Shader();

		this.material2 = new RawShaderMaterial({
			uniforms: {
				'map': {value: null},
				'occlusionMap': {value: this.occlusionMap},
				'color': {value: new Color(0xffffff)},
				'scale': {value: new Vector2()},
				'screenPosition': {value: new Vector3()}
			},
			vertexShader: this.shader.vertexShader,
			fragmentShader: this.shader.fragmentShader,
			blending: AdditiveBlending,
			transparent: true,
			depthWrite: false
		});

		this.mesh2 = new Mesh(this.geometry, this.material2);

		//

		this.screenPositionPixels = new Vector2();
		this.validArea = new Box2();
		this.viewport = new Vector4();
	};

	addElement(element) {
		this.elements.push(element)
	};

	onBeforeRender(renderer, scene, camera) {

		this.viewport.copy(renderer.getCurrentViewport());

		var invAspect = this.viewport.w / this.viewport.z;
		var halfViewportWidth = this.viewport.z / 2.0;
		var halfViewportHeight = this.viewport.w / 2.0;

		var size = 16 / this.viewport.w;
		this.scale.set(size * invAspect, size);

		this.validArea.min.set(this.viewport.x, this.viewport.y);
		this.validArea.max.set(this.viewport.x + (this.viewport.z - 16), this.viewport.y + (this.viewport.w - 16));

		// calculate position in screen space

		this.positionScreen.setFromMatrixPosition(this.matrixWorld);

		this.positionScreen.applyMatrix4(camera.matrixWorldInverse);
		this.positionScreen.applyMatrix4(camera.projectionMatrix);

		// horizontal and vertical coordinate of the lower left corner of the pixels to copy

		this.screenPositionPixels.x = this.viewport.x + (this.positionScreen.x * halfViewportWidth) + halfViewportWidth - 8;
		this.screenPositionPixels.y = this.viewport.y + (this.positionScreen.y * halfViewportHeight) + halfViewportHeight - 8;

		// screen cull

		if (this.validArea.containsPoint(this.screenPositionPixels)) {

			// save current RGB to temp texture

			renderer.copyFramebufferToTexture(this.screenPositionPixels, this.tempMap);

			// render pink quad

			var uniforms = this.material1a.uniforms;
			uniforms.scale.value = this.scale;
			uniforms.screenPosition.value = this.positionScreen;

			renderer.renderBufferDirect(camera, null, this.geometry, this.material1a, this.mesh1, null);

			// copy result to occlusionMap

			renderer.copyFramebufferToTexture(this.screenPositionPixels, this.occlusionMap);

			// restore graphics

			var uniforms = this.material1b.uniforms;
			uniforms.scale.value = this.scale;
			uniforms.screenPosition.value = this.positionScreen;

			renderer.renderBufferDirect(camera, null, this.geometry, this.material1b, this.mesh1, null);

			// render elements

			var vecX = -this.positionScreen.x * 2;
			var vecY = -this.positionScreen.y * 2;

			for (var i = 0, l = this.elements.length; i < l; i++) {

				var element = this.elements[i];

				var uniforms = this.material2.uniforms;

				uniforms.color.value.copy(element.color);
				uniforms.map.value = element.texture;
				uniforms.screenPosition.value.x = this.positionScreen.x + vecX * element.distance;
				uniforms.screenPosition.value.y = this.positionScreen.y + vecY * element.distance;

				var size = element.size / this.viewport.w;
				var invAspect = this.viewport.w / this.viewport.z;

				uniforms.scale.value.set(size * invAspect, size);

				this.material2.uniformsNeedUpdate = true;

				renderer.renderBufferDirect(camera, null, this.geometry, this.material2, this.mesh2, null);

			}

		}

	};

	dispose() {

		this.material1a.dispose();
		this.material1b.dispose();
		this.material2.dispose();

		this.tempMap.dispose();
		this.occlusionMap.dispose();

		for (var i = 0, l = elements.length; i < l; i++) {

			this.elements[i].texture.dispose();

		}

	};

	static Geometry() {

		const geometry = new BufferGeometry();

		const float32Array = new Float32Array([
			-1, -1, 0, 0, 0,
			1, -1, 0, 1, 0,
			1, 1, 0, 1, 1,
			-1, 1, 0, 0, 1
		]);

		const interleavedBuffer = new InterleavedBuffer(float32Array, 5);

		geometry.setIndex([0, 1, 2, 0, 2, 3]);
		geometry.addAttribute('position', new InterleavedBufferAttribute(interleavedBuffer, 3, 0, false));
		geometry.addAttribute('uv', new InterleavedBufferAttribute(interleavedBuffer, 2, 3, false));

		return geometry;
	};
};


class LensflareElement {
	constructor(texture, size, distance, color) {

		this.texture = texture;
		this.size = size || 1;
		this.distance = distance || 0;
		this.color = color || new Color(0xffffff);
	}

	static Shader() {
		return {

			uniforms: {

				'map': {value: null},
				'occlusionMap': {value: null},
				'color': {value: null},
				'scale': {value: null},
				'screenPosition': {value: null}

			},

			vertexShader: [

				'precision highp float;',

				'uniform vec3 screenPosition;',
				'uniform vec2 scale;',

				'uniform sampler2D occlusionMap;',

				'attribute vec3 position;',
				'attribute vec2 uv;',

				'varying vec2 vUV;',
				'varying float vVisibility;',

				'void main() {',

				'	vUV = uv;',

				'	vec2 pos = position.xy;',

				'	vec4 visibility = texture2D( occlusionMap, vec2( 0.1, 0.1 ) );',
				'	visibility += texture2D( occlusionMap, vec2( 0.5, 0.1 ) );',
				'	visibility += texture2D( occlusionMap, vec2( 0.9, 0.1 ) );',
				'	visibility += texture2D( occlusionMap, vec2( 0.9, 0.5 ) );',
				'	visibility += texture2D( occlusionMap, vec2( 0.9, 0.9 ) );',
				'	visibility += texture2D( occlusionMap, vec2( 0.5, 0.9 ) );',
				'	visibility += texture2D( occlusionMap, vec2( 0.1, 0.9 ) );',
				'	visibility += texture2D( occlusionMap, vec2( 0.1, 0.5 ) );',
				'	visibility += texture2D( occlusionMap, vec2( 0.5, 0.5 ) );',

				'	vVisibility =        visibility.r / 9.0;',
				'	vVisibility *= 1.0 - visibility.g / 9.0;',
				'	vVisibility *=       visibility.b / 9.0;',

				'	gl_Position = vec4( ( pos * scale + screenPosition.xy ).xy, screenPosition.z, 1.0 );',

				'}'

			].join('\n'),

			fragmentShader: [

				'precision highp float;',

				'uniform sampler2D map;',
				'uniform vec3 color;',

				'varying vec2 vUV;',
				'varying float vVisibility;',

				'void main() {',

				'	vec4 texture = texture2D( map, vUV );',
				'	texture.a *= vVisibility;',
				'	gl_FragColor = texture;',
				'	gl_FragColor.rgb *= color;',

				'}'

			].join('\n')

		};
	}
}

export {
	Lensflare,
	LensflareElement,
}
