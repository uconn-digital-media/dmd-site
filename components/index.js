import * as grid from './grid'
import * as icons from './icons'
import * as layout from './layout'
import * as ui from './ui'

export default {
	...grid,
	...icons,
	...layout,
	...ui
}